DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_u`	$$

CREATE PROCEDURE `contacts_u`(
	
	IN pTxt		VARCHAR(	100	)		,	-- contact name
	IN pTxt1	VARCHAR(	500	)		,	-- contact description
    IN pDt  	DATETIME 	        	,	-- update  datetime
    IN pId  	INT 	        		    -- contact id
	
)

BEGIN

	DECLARE lTxt	VARCHAR(	100			) DEFAULT TRIM(	pTxt		)	;
	DECLARE lTxt1	VARCHAR(	500 		) DEFAULT TRIM(	pTxt1		)	;
	
	DECLARE lNum	INT	;
	
	DECLARE EXIT handler FOR SQLEXCEPTION
	BEGIN
	
		ROLLBACK	;
		
		RESIGNAL	;
	
	END	;

	CALL contacts_isValidContactName(	lTxt	)	;
	
	CALL contacts_isDuplicatedContactName(	lTxt	)	;
	
	SET autocommit	= 0	;
	
	START TRANSACTION	;
	
		IF(

			lTxt <> '*'

		) THEN

			UPDATE cont_contact

				SET contactname	=	lTxt

				WHERE	(	
				
					id = pId
					
				)

			;

		END IF;
		
		IF(

			lTxt1 <> '*'

		) THEN

			UPDATE cont_contact

				SET description	=	lTxt1

				WHERE	(	
				
					id = pId
					
				)

			;

		END IF;

		IF(

			(lTxt <> '*') OR (lTxt1 <> '*')

		) THEN

			UPDATE cont_contact

			SET modified	=	pDt

			WHERE	(	
			
				id = pId
				
			)

		;

		END IF;
		
		SELECT ROW_COUNT() INTO lNum	;	-- rows affected
	
	COMMIT	;
	
	SELECT  lNum rowCount	;

END	$$

DELIMITER ;