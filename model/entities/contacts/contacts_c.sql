DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_c`	$$

CREATE PROCEDURE `contacts_c`(
	
	IN pTxt		VARCHAR(	100	)		,	-- contact name
	IN pTxt1	VARCHAR(	500	)		,	-- contact description
    IN pDt  	DATETIME 	        		-- create datetime
	
)

BEGIN

	DECLARE lTxt	VARCHAR(	100			) DEFAULT TRIM(	pTxt		)	;
	DECLARE lTxt1	VARCHAR(	500 		) DEFAULT TRIM(	pTxt1		)	;
	
	DECLARE lId	,   lId1	INT	;
	
	DECLARE EXIT handler FOR SQLEXCEPTION
	BEGIN
	
		ROLLBACK	;
		
		RESIGNAL	;
	
	END	;

	CALL contacts_isValidContactName(	lTxt	)	;
	
	CALL contacts_isDuplicatedContactName(	lTxt	)	;
	
	SET autocommit	= 0	;
	
	START TRANSACTION	;
	
		INSERT INTO cont_contact
		
			(

				contactname	,
				description	,
				created		,
				modified	

			)
			
			VALUES	(	
			
				lTxt	,
				IF(lTxt1 = '*', NULL, lTxt1),
				pDt		,
				pDt		
				
			)

		;
		
		SELECT LAST_INSERT_ID() INTO lId1	;	-- contact id
	
	COMMIT	;
	
	SELECT  lId1	lastId	;

END	$$

DELIMITER ;