
DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_l_ByName`	$$

CREATE PROCEDURE `contacts_l_ByName`(

    IN pTxt VARCHAR(100) -- contact name part

)

BEGIN

    DECLARE lTxt	VARCHAR(	100			) DEFAULT TRIM(	pTxt    )	;

    SELECT  id

        FROM cont_contact

        WHERE (

            contactname LIKE CONCAT('%', lTxt, '%')

        )
        
        ORDER BY contactname;

END	$$

DELIMITER ;