
DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_l`	$$

CREATE PROCEDURE `contacts_l`()

BEGIN

    SELECT  id
    
        FROM cont_contact
            
        ORDER BY contactname;

END	$$

DELIMITER ;