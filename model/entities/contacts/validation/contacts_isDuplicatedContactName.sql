DELIMITER $$

DROP PROCEDURE IF EXISTS `contacts_isDuplicatedContactName`	$$

CREATE PROCEDURE `contacts_isDuplicatedContactName`(

	IN pTxt varchar(	100	)
	
)

BEGIN

	DECLARE lTxt	VARCHAR(	100	) DEFAULT TRIM(	pTxt	)	;	
	DECLARE lId		INT	;
	
	SELECT		id INTO lId

		FROM 	cont_contact
		
		WHERE	(
			
			UCASE(	contactname	) = UCASE(	lTxt	)
			
		)

	;
	
	IF(
	
		lId IS NOT NULL
		
	) THEN
	
		SIGNAL SQLSTATE '45000' SET message_text = 'CONTACTS_DUPLICATEDNAME'	;
		
	END IF	;
	
END	$$

DELIMITER ;