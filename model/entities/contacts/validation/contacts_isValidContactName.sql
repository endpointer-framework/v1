DELIMITER $$

DROP PROCEDURE IF EXISTS `contacts_isValidContactName`	$$

CREATE PROCEDURE `contacts_isValidContactName`(

	IN pTxt VARCHAR(	100	)	-- contact name
	
)

BEGIN

	DECLARE lTxt VARCHAR(	100	) DEFAULT TRIM(	pTxt	)	;
	
	IF(
	
		(	char_length(	lTxt		) 	< 5	)
		
	) THEN
	
		SIGNAL SQLSTATE '45000' SET message_text = 'CONTACTS_INVALIDNAME'	;
		
	END IF	;
	
END	$$

DELIMITER ;
