DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_d`	$$

CREATE PROCEDURE `contacts_d`(
	
    IN pId  	INT 	        		    -- contact id
	
)

BEGIN

	DECLARE lNum	INT	;
	
	DECLARE EXIT handler FOR SQLEXCEPTION
	BEGIN
	
		ROLLBACK	;
		
		RESIGNAL	;
	
	END	;

	SET autocommit	= 0	;
	
	START TRANSACTION	;
		

		DELETE FROM  cont_contact

			WHERE	(	
			
				id = pId
				
			)

		;

		SELECT ROW_COUNT() INTO lNum	;	-- rows affected
	
	COMMIT	;
	
	SELECT  lNum rowCount	;

END	$$

DELIMITER ;