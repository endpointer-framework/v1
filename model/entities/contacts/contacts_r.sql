DELIMITER $$

DROP PROCEDURE IF EXISTS  `contacts_r`	$$

CREATE PROCEDURE `contacts_r`(
	
	IN pId		INT				-- contact id
	
)

BEGIN

	DECLARE lId		INT	;
	
	SELECT		id INTO lId

		FROM 	cont_contact

		WHERE	(
			
			id = pId
			
		)

	;
	
	IF(
	
		lId IS NULL
		
	) THEN
	
		SIGNAL SQLSTATE '45000' SET message_text = 'EP_DB_RECORDNOTFOUND'	;
		
	END IF	;

	SELECT *
	
		FROM cont_contact
		
		WHERE (

			id = lId

		);

END	$$

DELIMITER ;