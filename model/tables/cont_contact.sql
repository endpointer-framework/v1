DELIMITER $$

DROP TABLE IF EXISTS `cont_contact`;

CREATE TABLE `cont_contact` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contactname` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  
  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

DELIMITER ;
