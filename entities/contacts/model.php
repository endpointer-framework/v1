<?php

namespace com\endpointer\v1\entities\contacts\model;

use function com\endpointer\lib\framework\error\hasError;

//use function com\endpointer\v1\entities\contacts\mock\create as sCreate;
//use function com\endpointer\v1\entities\contacts\mock\update as sUpdate;
//use function com\endpointer\v1\entities\contacts\mock\delete as sDelete;
//use function com\endpointer\v1\entities\contacts\mock\retrieveAll as sRetrieveAll;
//use function com\endpointer\v1\entities\contacts\mock\retrieveByName as sRetrieveByName;
//use function com\endpointer\v1\entities\contacts\mock\retrieve as sRetrieve;

use function com\endpointer\v1\entities\contacts\db\create as sCreate;
use function com\endpointer\v1\entities\contacts\db\update as sUpdate;
use function com\endpointer\v1\entities\contacts\db\delete as sDelete;
use function com\endpointer\v1\entities\contacts\db\retrieveAll as sRetrieveAll;
use function com\endpointer\v1\entities\contacts\db\retrieveByName as sRetrieveByName;
use function com\endpointer\v1\entities\contacts\db\retrieve as sRetrieve;

//use function com\endpointer\v1\entities\contacts\mock\prepareServerData;
use function com\endpointer\v1\entities\contacts\rest\prepareServerData;

function delete() {

    if (hasError()) {
    } else {

        sDelete();
    }
}

function update() {

    prepareServerData();

    if (hasError()) {
    } else {

        sUpdate();
    }
}

function create() {

    prepareServerData();

    if (hasError()) {
    } else {

        sCreate();
    }
}

function retrieveByName() {

    sRetrieveByName();
}

function retrieveAll() {

    sRetrieveAll();
}

function retrieve() {

    sRetrieve();
}
