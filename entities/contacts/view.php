<?php

namespace com\endpointer\v1\entities\contacts\view;

use function com\endpointer\lib\framework\json\getJson;
use function com\endpointer\lib\framework\json\jsonEncode;
use function com\endpointer\lib\framework\json\setJson;

use function com\endpointer\v1\entities\contacts\state\getAccount;
use function com\endpointer\v1\entities\contacts\state\getAccounts;
use function com\endpointer\v1\entities\contacts\state\getLastId;
use function com\endpointer\v1\entities\contacts\state\getRowCount;

function sendLastId() {

    setJson(['lastId' => getLastId()]);

    jsonEncode();

    echo getJson();
}

function sendRowCount() {

    setJson(['rowCount' => getRowCount()]);

    jsonEncode();

    echo getJson();
}

function sendAccounts() {

    setJson(getAccounts());

    jsonEncode();

    echo getJson();
}

function sendAccount() {

    setJson(getAccount());

    jsonEncode();

    echo getJson();
}
