<?php

namespace com\endpointer\v1\entities\contacts\mock;

use function com\endpointer\lib\db\setLastId;
use function com\endpointer\lib\db\setRowCount;
use function com\endpointer\v1\entities\contacts\state\setAccount;
use function com\endpointer\v1\entities\contacts\state\setAccounts;
use function com\endpointer\v1\entities\contacts\state\setServerData;

function prepareServerData() {

    setServerData([

        'date' => '2020-10-04',
        'time' => '12:50:03'

    ]);
}

function create() {

    setLastId(1);
}

function update() {

    setRowCount(1);
}

function delete() {

    setRowCount(1);
}

function retrieveByName() {

    setAccounts([

        [
            'id' => 1,
            'name' => 'Judas Priest',
            'description' => 'Greatest Heavy Metal Band of The Word!',
        ]

    ]);
}

function retrieveAll() {

    setAccounts([

        [
            'id' => 1,
            'name' => 'Judas Priest',
            'description' => 'Greatest Heavy Metal Band of The Word!',
        ]

    ]);
}

function retrieve() {

    setAccount([

        'id' => 1,
        'name' => 'Judas Priest',
        'description' => 'Greatest Heavy Metal Band of The Word!',

    ]);
}
