<?php

namespace com\endpointer\v1\entities\contacts;

use const com\endpointer\v1\entities\config\general\CFG;

use function com\endpointer\lib\framework\config\setConfig;

use function com\endpointer\lib\framework\request\getVerb;
use function com\endpointer\lib\framework\request\hasUrlField;
use function com\endpointer\lib\framework\request\getUrlField;

use function com\endpointer\lib\framework\error\hasError;

use function com\endpointer\lib\framework\request\getBodyData;

use function com\endpointer\v1\entities\contacts\state\setId;
use function com\endpointer\v1\entities\contacts\state\setName;
use function com\endpointer\v1\entities\contacts\state\setContactName;
use function com\endpointer\v1\entities\contacts\state\setDescription;

use function com\endpointer\v1\entities\contacts\model\create;
use function com\endpointer\v1\entities\contacts\model\delete as sDelete;
use function com\endpointer\v1\entities\contacts\model\update;
use function com\endpointer\v1\entities\contacts\model\retrieve;
use function com\endpointer\v1\entities\contacts\model\retrieveByName;
use function com\endpointer\v1\entities\contacts\model\retrieveAll;

use function com\endpointer\v1\entities\contacts\view\sendAccount;
use function com\endpointer\v1\entities\contacts\view\sendAccounts;
use function com\endpointer\v1\entities\contacts\view\sendLastId;
use function com\endpointer\v1\entities\contacts\view\sendRowCount;

function controller() {

    prepare();

    if (hasError()) {

        release();

        return;
    }

    switch (getVerb()) {

        case 'GET':

            get();

            break;

        case 'POST':

            post();

            break;

        case 'PUT':

            put();

            break;

        case 'DELETE':

            delete();

            break;
    }

    release();
}

function prepare() {

    setConfig(CFG);

    \com\endpointer\lib\db\connect();
}

function release() {

    \com\endpointer\lib\db\disconnect();
}

function get() {

    if (hasUrlField('id')) {

        setId(

            getUrlField('id')
        );

        retrieve();

        if (hasError()) {
        } else {

            sendAccount();
        }
    } elseif (hasUrlField('name')) {

        setName(getUrlField('name'));

        retrieveByName();

        if (hasError()) {
        } else {

            sendAccounts();
        }
    } else {

        retrieveAll();

        if (hasError()) {
        } else {

            sendAccounts();
        }
    }
}

function post() {

    setContactName(

        isset(getBodyData()['name']) ? getBodyData()['name'] : '*'

    );

    setDescription(

        isset(getBodyData()['description']) ? getBodyData()['description'] : '*'

    );

    create();

    if (hasError()) {
    } else {

        sendLastId();
    }
}

function put() {

    setId(getUrlField('id'));

    setContactName(

        isset(getBodyData()['name']) ? getBodyData()['name'] : '*'

    );

    setDescription(

        isset(getBodyData()['description']) ? getBodyData()['description'] : '*'

    );

    update();

    if (hasError()) {
    } else {

        sendRowCount();
    }
}

function delete() {

    setId(getUrlField('id'));

    sDelete();

    if (hasError()) {
    } else {

        sendRowCount();
    }
}
