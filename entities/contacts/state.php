<?php

namespace com\endpointer\v1\entities\contacts\state;

use const com\endpointer\v1\entities\contacts\config\constants\ID;
use const com\endpointer\v1\entities\contacts\config\constants\ACCOUNTS;
use const com\endpointer\v1\entities\contacts\config\constants\ACCOUNT;
use const com\endpointer\v1\entities\contacts\config\constants\CONTACTNAME;
use const com\endpointer\v1\entities\contacts\config\constants\DESCRIPTION;
use const com\endpointer\v1\entities\contacts\config\constants\SERVER_DATA;
use const com\endpointer\v1\entities\contacts\config\constants\NAME;

use function com\endpointer\lib\db\getLastId as sGetLastId;
use function com\endpointer\lib\db\getRowCount as sGetRowCount;
use function com\endpointer\lib\framework\state\setLocalState;
use function com\endpointer\lib\framework\state\getLocalState;

function setId($v) {

    setLocalState(ID, $v);
}

function setName($v) {

    setLocalState(NAME, $v);
}

function setContactName($v) {

    setLocalState(CONTACTNAME, $v);
}

function setDescription($v) {

    setLocalState(DESCRIPTION, $v);
}

function setAccounts($v) {

    setLocalState(ACCOUNTS, $v);
}

function setAccount($v) {

    setLocalState(ACCOUNT, $v);
}

function setServerData($v) {

    setLocalState(SERVER_DATA, $v);
}

function getId() {

    return getLocalState(ID);
}

function getName() {

    return getLocalState(NAME);
}

function getContactName() {

    return getLocalState(CONTACTNAME);
}

function getDescription() {

    return getLocalState(DESCRIPTION);
}

function getAccounts() {

    return getLocalState(ACCOUNTS);
}

function getAccount() {

    return getLocalState(ACCOUNT);
}

function getServerData() {

    return getLocalState(SERVER_DATA);
}

function getLastId() {

    return sGetLastId();
}

function getRowCount() {

    return sGetRowCount();
}
