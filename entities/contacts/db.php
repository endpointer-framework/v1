<?php

namespace com\endpointer\v1\entities\contacts\db;

use function com\endpointer\lib\db\create as sCreate;
use function com\endpointer\lib\db\update as sUpdate;
use function com\endpointer\lib\db\delete as sDelete;
use function com\endpointer\lib\db\getIdList;
use function com\endpointer\lib\db\getParams;
use function com\endpointer\lib\db\getRec;
use function com\endpointer\lib\db\idlist;
use function com\endpointer\lib\db\read;
use function com\endpointer\lib\db\setParams;
use function com\endpointer\lib\db\setProcName;

use function com\endpointer\lib\framework\error\hasError;

use function com\endpointer\v1\entities\contacts\state\getContactName;
use function com\endpointer\v1\entities\contacts\state\getDescription;
use function com\endpointer\v1\entities\contacts\state\getId;
use function com\endpointer\v1\entities\contacts\state\getName;
use function com\endpointer\v1\entities\contacts\state\getServerData;
use function com\endpointer\v1\entities\contacts\state\setAccount;
use function com\endpointer\v1\entities\contacts\state\setAccounts;

function delete() {

    setProcName('contacts_d');

    setParams([

        'id' => getId()

    ]);

    sDelete();

    if (hasError()) {

        return;
    }
}

function update() {

    setProcName('contacts_u');

    setParams([

        'contactname' => getContactName(),
        'description' => getDescription(),
        'created' => getDateTime(),
        'id' => getId()

    ]);

    sUpdate();

    if (hasError()) {

        return;
    }
}

function create() {

    setProcName('contacts_c');

    setParams([

        'contactname' => getContactName(),
        'description' => getDescription(),
        'created' => getDateTime()

    ]);

    sCreate();

    if (hasError()) {

        return;
    }
}

function retrieve() {

    setProcName('contacts_r');

    setParams([

        'id' => getId()

    ]);

    read();

    if (hasError()) {

        return;
    }

    setAccount([

        'id' => getRec()['id'],
        'name' => getRec()['contactname'],
        'description' => getRec()['description']


    ]);
}

function retrieveAll() {

    setProcName('contacts_l');

    prepareAccounts();
}

function retrieveByName() {

    setProcName('contacts_l_byName');

    setParams([

        'contactname' => getName()

    ]);

    prepareAccounts();
}

function getDateTime() {

    $sd = getServerData();

    return "{$sd['date']} {$sd['time']}";
}

function prepareAccounts() {

    idlist();

    if (hasError()) {

        return;
    }

    $a = [];

    foreach (getIdList()

        as $id) {

        setProcName('contacts_r');

        setParams([

            'id' => $id

        ]);

        read();

        if (hasError()) {

            return;
        }

        $a[] = [

            'id' => getRec()['id'],
            'name' => getRec()['contactname'],
            'description' => getRec()['description']


        ];
    }

    setAccounts($a);
}
