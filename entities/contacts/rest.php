<?php

namespace com\endpointer\v1\entities\contacts\rest;

use const com\endpointer\v1\entities\contacts\config\general\CFG;

use function com\endpointer\lib\framework\curl\get;
use function com\endpointer\lib\framework\curl\setUrl;

use function com\endpointer\lib\framework\json\getJson;

use function com\endpointer\lib\framework\error\hasError;
use function com\endpointer\lib\framework\response\setServerErrorHeader;
use function com\endpointer\v1\entities\contacts\state\setServerData;

function prepareServerData() {

    $h = CFG['URL']['host'];
    $u = CFG['URL']['uri'];

    setUrl("$h$u");

    get();

    if (hasError()) {

        setServerErrorHeader();
    } else {

        setServerData(getJson());
    }
}
