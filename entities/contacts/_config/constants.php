<?php

namespace com\endpointer\v1\entities\contacts\config\constants;

const ID = 'ID';
const NAME = 'NAME';
const CONTACTNAME = 'CONTACTNAME';
const DESCRIPTION = 'DESCRIPTION';
const ACCOUNTS = 'ACCOUNTS';
const ACCOUNT = 'ACCOUNT';
const SERVER_DATA = 'SERVER_DATA';
