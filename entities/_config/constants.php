<?php

namespace com\endpointer\v1\entities\config\constants;

const CONTACTS_INVALIDNAME = 'CONTACTS_INVALIDNAME';
const CONTACTS_DUPLICATEDNAME = 'CONTACTS_DUPLICATEDNAME';
