<?php

namespace com\endpointer\v1\services\serverdata\view;

use const com\endpointer\config\general\EP_EP_STATUS;

use function com\endpointer\lib\framework\json\getJson;
use function com\endpointer\lib\framework\json\jsonEncode;
use function com\endpointer\lib\framework\json\setJson;

use function com\endpointer\v1\services\serverdata\state\getDate;
use function com\endpointer\v1\services\serverdata\state\getTime;

function sendServerData() {

    setJson([

        'date' => getDate(),
        'time' => getTime()

    ]);

    jsonEncode();

    echo getJson();
}
