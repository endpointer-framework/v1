<?php

namespace com\endpointer\v1\services\serverdata;

use function com\endpointer\lib\framework\request\getVerb;

use function com\endpointer\lib\framework\error\hasError;

use function com\endpointer\v1\services\serverdata\model\getServerData;

use function com\endpointer\v1\services\serverdata\view\sendServerData;

function controller() {

    prepare();

    if (hasError()) {

        release();

        return;
    }

    switch (getVerb()) {

        case 'GET':

            get();

            break;
    }

    release();
}

function prepare() {
}

function release() {
}

function get() {

    getServerData();

    sendServerData();
}
