<?php

namespace com\endpointer\v1\services\serverdata\state;

use const com\endpointer\v1\services\serverdata\config\constants\DATE;
use const com\endpointer\v1\services\serverdata\config\constants\TIME;

use function com\endpointer\lib\framework\state\setLocalState;
use function com\endpointer\lib\framework\state\getLocalState;

function setDate($v) {

    setLocalState(DATE, $v);
}

function getDate() {

    return getLocalState(DATE);
}

function setTime($v) {

    setLocalState(TIME, $v);
}

function getTime() {

    return getLocalState(TIME);
}
